# -*- coding: utf-8 -*-
"""
Created on Sat May 29 15:14:01 2021

@author: Zeynep
"""

import pandas as pd

df=pd.read_csv("finaltenis.csv")

inputs=df.iloc[:,0:6]
o=df.iloc[:,6:7]

from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(inputs,o,test_size=0.3,random_state=0)


#decision tree
from sklearn.tree import DecisionTreeClassifier 
from sklearn.metrics import confusion_matrix,classification_report
dtc = DecisionTreeClassifier(criterion= 'gini') 
dtc.fit(x_train,y_train) 
y_pred = dtc.predict(x_test)
cm= confusion_matrix(y_test,y_pred) 
print(classification_report(y_test,y_pred))

#random forest classifier
from sklearn.ensemble import RandomForestClassifier
rfc = RandomForestClassifier(n_estimators=5,criterion = 'entropy') 
rfc.fit(x_train,y_train) 

y_pred2 = rfc.predict(x_test) 
cm2 = confusion_matrix(y_test,y_pred2) 
print(classification_report(y_test,y_pred2)) 