# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 12:13:57 2021

@author: Zeynep
"""

import pandas as pd

df=pd.read_csv("finaltenis.csv")

inputs=df.iloc[:,0:6]
o=df.iloc[:,6:7]

from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(inputs,o,test_size=0.3,random_state=0)

"""scaling of data
from sklearn.preprocessing import StandardScaler
sc=StandardScaler()

X_train=sc.fit_transform(x_train)
X_test=sc.transform(x_test)
print(X_test) """


from sklearn.linear_model import LogisticRegression

logr=LogisticRegression(random_state=0);
logr.fit(x_train,y_train)

y_predict=logr.predict(x_test)

predict=pd.DataFrame(y_predict)

from sklearn.metrics import confusion_matrix,classification_report
c=confusion_matrix(y_test,y_predict)
print(classification_report(y_test,y_predict))
