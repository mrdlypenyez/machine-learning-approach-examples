
import pandas as pd
import numpy as np
data=pd.read_csv("Automobile_data.csv")

#data.drop([9,10],axis=0,inplace=True)

#we deleted the "?" sections in the data set so that there are no possible errors in model training.

def bul(des):
    if "?" in des:
        return True
    return False 

for i in range(0,195):
    a=data.iloc[i:i+1,:].apply(bul)
    

#data editing and analysis
x1=data.iloc[:,3:5]
x2=data.iloc[:,10:14]
x3=data.iloc[:,25:26].dropna()

x=pd.concat([x1,x2,x3],axis=1)


#convert characteristic elements numerically

from sklearn.preprocessing import LabelEncoder
lb=LabelEncoder() 

for i in range(0,2):   
    x.iloc[:,i:i+1]=lb.fit_transform(x.iloc[:,i:i+1])



y=data.iloc[:,2:3] # label encoder 

y["make"]=lb.fit_transform(y["make"])


#naive bayes algorithm

from sklearn.naive_bayes import GaussianNB

gnb=GaussianNB()

#gaussian model training
gnb.fit(x,y)

#brand model predict-> masta model automobil->feature gaz std width:2337 .. price 13950
y_pred=gnb.predict(np.array([1,0,176,66,54,2337,13950]).reshape(1,7))
print(y_pred)

print(gnb.predict_proba(np.array([1,0,176,66,54,2337,13950]).reshape(1,7)))




















