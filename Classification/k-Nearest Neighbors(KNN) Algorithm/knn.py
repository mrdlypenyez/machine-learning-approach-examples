# -*- coding: utf-8 -*-
"""
Created on Thu Apr 29 11:16:08 2021

@author: Zeynep
"""
from sklearn.preprocessing import LabelEncoder
from sklearn.preprocessing import OneHotEncoder
import pandas as pd
import numpy as np



df=pd.read_csv("tenis.csv")

#Tennis playing status according to weather conditions and other parameters

o=df.iloc[:,0:1]

ohe=OneHotEncoder()
o=ohe.fit_transform(o).toarray()
df2=pd.DataFrame(o,columns=["overcast","rainy","sunny"])


#for windy encoder
le=LabelEncoder()
df3=df.iloc[:,3:4]
df3=le.fit_transform(df3)
df3=pd.DataFrame(df3,columns=["windy"])

#for play encoder
df4=df.iloc[:,4:5]
df4=le.fit_transform(df4)
df4=pd.DataFrame(df4,columns=["play"])

df.drop(['outlook','windy','play'],axis=1, inplace=True)

#merge numerical column

inputs=pd.concat([df,df2,df3], axis=1)
outputs=df4  #numerical play state

from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(inputs,outputs,test_size=0.3,random_state=0)

from sklearn.neighbors import KNeighborsClassifier
knn=KNeighborsClassifier(n_neighbors=4,metric="minkowski")
knn.fit(x_train,y_train)
y_pred=knn.predict(x_test)

from sklearn.metrics import confusion_matrix,classification_report
cn=confusion_matrix(y_test,y_pred)
print(cn)

#export processef dataframe
#data=pd.concat([df,df2,df3,df4], axis=1)
#data.to_csv("finaltenis.csv",index=False)

#Finding the best parameter estimate for the model
from sklearn.model_selection import GridSearchCV 
par={"n_neighbors":np.arange(1,7),"metric":["manhattan","minkowski"]} 
from sklearn.neighbors import KNeighborsClassifier 
knn = KNeighborsClassifier() 
knn_cv=GridSearchCV(knn,par,cv=3)
knn_cv.fit(x_train,y_train) 
print("Best Model:paramters") 
best=knn_cv.best_params_
print(best) 

# # Cross Validation 
from sklearn.model_selection import cross_val_score
CD=cross_val_score(knn,x_train,y_train,cv=5) 
print(CD) 
print(np.mean(CD)) 























