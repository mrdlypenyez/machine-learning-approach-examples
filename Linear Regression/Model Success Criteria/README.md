# Model Success Criteria

| CRITERIA                     |
|------------------------------|
| R-SQUARED                    |
| ADJUSTED R-SQUARED           |
| RMSE(Root Mean Square Error) |
| MALLOW CP                    |
| AIC(Akaike Inf. Crit.)       |
| BIC(Bayes Inf. Crit.)        |


## R-SQUARED   
The predicted R-squared indicates how well a regression model predicts responses for new observations. This statistic helps you determine when the model fits the original data but is less capable of providing valid predictions for new observations

- Formula
| R2Score=1-(RSS/TSS)
--|

RSS:Resuidal sum of square
TSS:Total sum of square


## ADJUSTED R-SQUARED

Adjusted R-squared compares the explanatory power of regression models that contain different numbers of predictors.
The adjusted R-squared is a modified version of R-squared that has been adjusted for the number of predictors in the model.It is always lower than the R-squared.

| Adj_R2Score=(RSS/(n-d-1))/(TSS/(n-1)) 
 -| 

n=number of observations
d=independent variable number

## RMSE(Root Mean Square Error)
MSE is a good measure of accuracy, but only to compare prediction errors of different models or model configurations for a particular variable and not between variables, as it is scale-dependent.

## Mallow CP

Is used to assess the fit of a regression model that has been estimated using ordinary least squares. It is applied in the context of model selection, where a number of predictor variables are available for predicting some outcome, and the goal is to find the best model involving a subset of these predictors.

| CP=(RSS+2*d*var)/n
 -|
var=Resuidal sum of square variance

## AIC
The Akaike information criterion (AIC) is an estimator of prediction error and thereby relative quality of statistical models for a given set of data.Given a collection of models for the data, AIC estimates the quality of each model, relative to each of the other models. Thus, AIC provides a means for model selection

|AIC=(RSS+2*d*var)/(n*var)
 -|

## BIC
Is a criterion for model selection among a finite set of models.

| BIC=(RSS+In(n)*d*var)/(n*var)
 -|












