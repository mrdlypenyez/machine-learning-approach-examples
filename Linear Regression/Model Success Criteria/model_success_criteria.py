import pandas as pd
import numpy as np
df=pd.read_excel("data.xlsx")


from sklearn.model_selection import train_test_split

x_train,x_test,y_train,y_test=train_test_split(df[["LSTAT"]],df.Price,test_size=0.3,random_state=0)

#model building(LINEAR REGRESSION)
from sklearn.linear_model import LinearRegression
lr=LinearRegression()
#train data
lr.fit(x_train,y_train)
prediction=lr.predict(x_test)

x1=x_train.values
x2=x_test.values
y1=y_train.values
y2=y_test.values

#Model Success Criteria

#1)R2 SCORE MANUEL

# r2score->1-(rss/tss)
rss=sum((y2-prediction)**2)
tss=sum((y2-np.mean(y2))**2)

R2=1-(rss/tss)
print(R2)

#2) MANUEL Adj_R2_Score

n=len(y2) #observation
d=x2.shape[1]#number of independent variables

adj_R2=1-(rss/(n-d-1))/(tss/(n-1))
print(adj_R2)


#3) MANUEL RMSE
RMSE=np.sqrt(rss/n)
print(RMSE)

#-> CODE RMSE
from sklearn.metrics import mean_squared_error
rmse=np.sqrt(mean_squared_error(prediction,y_test))
print(rmse)



#4)MALLOW CP
var=np.var(y2-prediction)
cp=(rss+2*d*var)/n
print(cp)


#5)BIC
bic=(rss+np.log(n)*d*var)/(n*var)
print(bic)








