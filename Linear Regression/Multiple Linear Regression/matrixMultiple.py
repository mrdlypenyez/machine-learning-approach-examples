
#Multiple linear regression manual as matrix
#B=(X'.X)^-1.X'.Y

import pandas as pd 
import numpy as np

df=pd.read_excel("data.xlsx")
X=df.iloc[:,0:2]
Y=df.iloc[:,2:3]

n=len(df)
s=np.ones((n,1)) #Matrix consisting of n of dimension 1

"""Dataframe merge
a=pd.DataFrame(s)
X2=pd.concat([a,x],axis=1)
"""

X=np.concatenate([s,X],axis=1) #merge columns

A=np.dot(X.transpose(),X) #x transpose * itself product

A2=np.linalg.inv(A) #Invert this matrix
A3=np.dot(X.transpose(),Y)

B=np.dot(A2,A3) 

#Y_predict=beta0+beta1x1+beta2*x2+...
#predict using coefficients->katsaıları kullanarak tahmin etme
y_predict=X.dot(B)


#manuel r2score success value
c=y_predict
y=Y.values

rss=sum((y-c)**2)
tss=sum((y-np.mean(y))**2)

R2=1-(rss/tss)
print(R2)
#RMSE ile succes criteria 

from sklearn.metrics import mean_squared_error
rmse=np.sqrt(mean_squared_error(c,y))
print("rmse succes:c",rmse)
























