from sklearn.metrics import r2_score
import pandas as pd
import numpy as np
from sklearn import datasets

df=datasets.load_boston()

inputs=pd.DataFrame(df.data)
inputs.columns=df.feature_names 

output=pd.DataFrame(df.target)
output.columns=["Price"]

#merge inputs and outputs by column
data=pd.concat([inputs,output],axis=1)



#multiple linear regression
from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(inputs,output,test_size=0.3,random_state=0)

from sklearn.linear_model import LinearRegression
lr=LinearRegression()
lr.fit(x_train,y_train)

prediction=lr.predict(x_test)

print("Multiple Linear Regression value:")
print(r2_score(y_test,prediction))

#Cross Validation
from sklearn.model_selection import cross_val_score
kfold=cross_val_score(lr,x_train,y_train,cv=5)
print(kfold)
print(np.mean(kfold))





