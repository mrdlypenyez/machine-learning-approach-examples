from sklearn.metrics import r2_score
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression 
from sklearn import datasets

df=datasets.load_boston()

inputs=pd.DataFrame(df.data)
inputs.columns=df.feature_names 

output=pd.DataFrame(df.target)
output.columns=["Price"]
data=pd.concat([inputs,output],axis=1)


from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(inputs,output,test_size=0.3,random_state=0)



#Polynomial Regression

from sklearn.preprocessing import PolynomialFeatures

poly_reg=PolynomialFeatures(degree=2)

x_poly=poly_reg.fit_transform(x_train) #train and changed

lin_reg=LinearRegression()
lin_reg.fit(x_poly,y_train)

prediction=lin_reg.predict(poly_reg.fit_transform(x_test))

print("Polynomial R2 Value: ")
print(r2_score(y_test,lin_reg.predict(poly_reg.fit_transform(x_test))))


#cross validation column success ration
from sklearn.model_selection import cross_val_score
crV=cross_val_score(lin_reg,x_train,y_train,cv=10)
print(crV)
print(np.mean(crV))

