# Sum of Least Squares Method


It is to find the line in the least squares sum such that the sum of the residuals is minimum.

## Assumptions:

- Sum residuals of the terms  should be brought closer to zero.

   E(e)=0               e:residuals term  

- The covariance between the independent variable and the error terms is equal to zero.


  CoV(x,e) =E(xe)=0


## Formula

![slsmain](https://gitlab.com/mrdlypenyez/machine-learning-approach-examples/-/raw/master/Linear%20Regression/Sum%20of%20Least%20Squares%20Method/formula/r1.png)


| Yi is the predicted value of the dependent variable (y) for any given value of the independent variable (x) |
|-------------------------------------------------------------------------------------------------------------|


| Bo is the intercept, the predicted value of y when the x is 0. |
|----------------------------------------------------------------|

![bo](https://gitlab.com/mrdlypenyez/machine-learning-approach-examples/-/raw/master/Linear%20Regression/Sum%20of%20Least%20Squares%20Method/formula/b0.png)


| B1 is the regression coefficient how much we expect y to change as x increases. |
|-----------------------------------------------------------------------------------|


![b1](https://gitlab.com/mrdlypenyez/machine-learning-approach-examples/-/raw/master/Linear%20Regression/Sum%20of%20Least%20Squares%20Method/formula/b1.png)

![xy](https://gitlab.com/mrdlypenyez/machine-learning-approach-examples/-/raw/master/Linear%20Regression/Sum%20of%20Least%20Squares%20Method/formula/xy.png)
 


