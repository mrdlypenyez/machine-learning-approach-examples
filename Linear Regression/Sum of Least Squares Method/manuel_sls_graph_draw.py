import pandas as pd

data=pd.read_excel("data.xlsx")

#Manuel Drawing

import matplotlib.pyplot as plt
Price=data["Price"].values
LSTAT=data["LSTAT"].values

plt.scatter(LSTAT,Price,color="blue")
plt.xlabel("Ratio of purchasing power to population")
plt.ylabel("House prices")

#plt.plot(x2,prediction_manuel,color="red")

#graph drawing with code

#state 1
import seaborn as sns 
sns.lmplot(x="LSTAT",y="Price",data=data)

#state 2
iris=sns.load_dataset("iris")
sns.lmplot(x="sepal_length",y="sepal_width",hue="species",data=iris)

