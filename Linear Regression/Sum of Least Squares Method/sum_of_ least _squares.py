from sklearn.metrics import r2_score
import pandas as pd
import numpy as np
from sklearn import datasets

df=datasets.load_boston()

inputs=pd.DataFrame(df.data)
inputs.columns=df.feature_names 

output=pd.DataFrame(df.target)
output.columns=["Price"]

#merge inputs and outputs by column
data=pd.concat([inputs,output],axis=1)

#export processef dataframe
data.to_excel("data.xlsx",index=False)
data.to_csv("data1.csv",index=False)


#Drawing graphics
import matplotlib.pyplot as plt
plt.scatter(data["LSTAT"],data["Price"])
plt.xlabel("Ratio of purchasing power to population")
plt.ylabel("House prices")
  
#dividing data for training and testing

from sklearn.model_selection import train_test_split

x_train,x_test,y_train,y_test=train_test_split(inputs[["LSTAT"]],output,test_size=0.3,random_state=0)

#model building(LINEAR REGRESSION)
from sklearn.linear_model import LinearRegression
lr=LinearRegression()
#train data
lr.fit(x_train,y_train)

prediction=lr.predict(x_test)


print("Linear R2 value: ")
print(r2_score(y_test,prediction))


print(lr.coef_)  #beta 1 coefficient
print(lr.intercept_) #beta 0 coefficient

#Manuel Sum of Least Squares(SLS)

x1=x_train.values
x2=x_test.values

y1=y_train.values
y2=y_test.values

a1=sum((x1-np.mean(x1))*(y1-np.mean(y1)))
a2=sum((x1-np.mean(x1))**2)

beta1=a1/a2
print(beta1)

beta0=np.mean(y1)-beta1*np.mean(x1)
print(beta0)

#Function -> y=beta0+beta1*x
prediction_manuel=beta0+beta1*x_test

#Manuel Drawing

Price=data["Price"].values
LSTAT=data["LSTAT"].values

plt.scatter(LSTAT,Price,color="blue")
plt.xlabel("Ratio of purchasing power to population")
plt.ylabel("House prices")

plt.plot(x2,prediction_manuel,color="red")

 
