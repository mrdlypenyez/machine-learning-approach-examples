import pandas as pd
import numpy as np 
from sklearn.metrics import r2_score
from sklearn.linear_model import LinearRegression 
from sklearn import datasets

df=datasets.load_boston()

inputs=pd.DataFrame(df.data)
inputs.columns=df.feature_names 

output=pd.DataFrame(df.target)
output.columns=["Price"]
data=pd.concat([inputs,output],axis=1)

from sklearn.model_selection import train_test_split
x_train,x_test,y_train,y_test=train_test_split(inputs,output,test_size=0.3,random_state=0)

#Decision Tree
from sklearn.tree import DecisionTreeRegressor
dt=DecisionTreeRegressor()
dt.fit(x_train,y_train)

pr=dt.predict(x_test)

print("Decision Tree R2 Value: ")
print(r2_score(y_test,pr))

#Random Forest Regression

from sklearn.ensemble import RandomForestRegressor
#n_enstimator:the number of trees it will create
rf=RandomForestRegressor(n_estimators=20,random_state=0)
rf.fit(x_train,y_train)

pr2=rf.predict(x_test)

print("Random Forest R2 Value: ")
print(r2_score(y_test,pr2))




















