## Machine Learning Approach Examples

The machine learning discipline uses a variety of approaches to teach computers to perform tasks for which no fully satisfactory algorithm is available.

# Linear Regression
Linear regression is a linear approach to modelling the relationship between a scalar response and one or more explanatory variables

| Approach                    |
|-----------------------------|
| Sum of Least Squares Method |
| Multiple Linear Regression  |
| Tree Algorithm              |
| Model Success Criteria      |

